from sklearn import tree, preprocessing

sample_encoder = preprocessing.LabelEncoder()
sample_encoder.fit(["Sunny", "Warm", "Low", "Near", "Medium", "Rain", "Cold", "Strong", "Far"])

label_encoder = preprocessing.LabelEncoder()
label_encoder.fit(["Yes", "No"])

train_dataset = [["Sunny", "Warm", "Low", "Near"],
                 ["Sunny", "Warm", "Medium", "Near"],
                 ["Rain", "Cold", "Strong", "Near"],
                 ["Sunny", "Warm", "Low", "Far"],
                 ["Sunny", "Warm", "Medium", "Far"]]
labels = ["Yes", "Yes", "No", "No", "Yes"]

test_dataset = [["Sunny", "Cold", "Low", "Near"]]

def __get_train_dataset():
     processed_train_dataset = []
     #convert each attribute values and output values to  numeric numbers as sklearn support only number
     for row in train_dataset :
         processed_row = sample_encoder.transform(row)
         processed_train_dataset.append(processed_row)

     return processed_train_dataset,label_encoder.transform(labels)

def __get_train_decision_tree(trained_dataset, labels) :
    classifer = tree.DecisionTreeClassifier()
    return classifer.fit(trained_dataset, labels)

def __get_output(model):
    processed_test_dataset = []
    for row in test_dataset:
        processed_row = sample_encoder.transform(row)
        processed_test_dataset.append(processed_row)
    output = model.predict(processed_test_dataset)
    return label_encoder.inverse_transform(output)

if __name__ == '__main__':

    try:
        print "Processed Train dataset generating ...."
        (train_dataset , labels) = __get_train_dataset()
        print "Processed Train dataset generation finished !!!"

        print "Train decision tree using the train dataset ....."
        model = __get_train_decision_tree(train_dataset, labels)
        print  "Decision tree trained !!!"

        output = __get_output(model)
        print "prediction : " + str(output)
    except ValueError, e:
        print e.message

